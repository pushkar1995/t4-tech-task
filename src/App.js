import './App.css';
import { BrowserRouter, Route, Link } from 'react-router-dom'
import Header from './components/Header'
import Button from './components/Button';
import SignupPage from './components/SignupPage/SignupPage'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
          <header className="App-header">
           <Header />
          </header>
        <Route path="/" exact component={SignupPage} />
        <Route path="/button" component={Button} />
      </div>
    </BrowserRouter>
    
  );
}

export default App;
