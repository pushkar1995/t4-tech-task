// Solve the following problem in Javascript

// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17
// Find the sum of all the primes below two million.

// Submission: A program that solves this problem.


let primeSummation = (n) => {

    let sum = 0

    let arrayToSum = [0,0]
    let i 
    for(i = 2; i < n; i ++){
        arrayToSum.push(i)
    }

    for(i = 2; i < n; i ++){

        if(arrayToSum[i] != 0){
            let j 
            for (j =(2 * i); j < n; j += i){
                arrayToSum[j] = 0
            }
        }
    }

    for(i = 0; i < arrayToSum.length; i ++){

        if(arrayToSum[i] != 0){
            console.log('Added prime' + arrayToSum[i] + ', sum is now ' + sum)
            }
            sum = sum + arrayToSum[i]
        }

    return sum
}

console.log('Result is ' + primeSummation(2000000))