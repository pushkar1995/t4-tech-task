const Button = (props) => {
    const { type, text } = props;
    const defaultButtonStyle = {
      width: 'max-content',
      padding: '10px',
      textTransform: 'uppercase',
      borderRadius: '3px',
      marginButtom: '10px',
    }
    const colorOptions = {
        'default': { backgroundColor: '#bbb', color:'black'},
        'primary': { backgroundColor: '#4a4aea', color:'white'},
        'secondary': { backgroundColor: '#ef5a75', color:'white'},
        'disabled': { backgroundColor: '#bbb', color:'#888484', pointerEvents: 'none', cursor: "not-allowed"},
    }
    const buttonStyle = Object.assign({}, defaultButtonStyle, colorOptions[type])
    return (
        <div style = {buttonStyle}>{text}</div>
    )
}
export default Button;

