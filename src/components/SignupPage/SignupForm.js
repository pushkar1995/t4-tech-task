import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from './ButtonForSignup'
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({

    fieldsandButtonStyle: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: "center",
        marginTop: "1rem",
        marginBottom: "1rem"
    },
  }));

function SignupForm(props) {
    const classes = useStyles();
    return (
        <div>
            <form>
                <div>
                    <div className={classes.fieldsandButtonStyle}>
                        <TextField id="outlined-basic" label="Name *" variant="outlined" />
                    </div>
                    <div className={classes.fieldsandButtonStyle}>
                        <TextField id="outlined-basic" label="Email *" variant="outlined" />
                    </div>
                    <div className={classes.fieldsandButtonStyle}>
                        <TextField id="outlined-basic" label="Password *" variant="outlined" />
                    </div>

                    <div className={classes.fieldsandButtonStyle}>
                        <Button type="register" text="REGISTER"/>
                    </div>
                </div>
            </form> 
        </div>
    );
}

export default SignupForm;