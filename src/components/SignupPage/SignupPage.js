import React from 'react';
import Button from './ButtonForSignup'
import { makeStyles } from "@material-ui/core/styles";
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import DividerWithText from '../DividerWithText'
import SignupForm from './SignupForm'


const useStyles = makeStyles(theme => ({
    mainContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    fieldStyles: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: "center",
        marginTop: "1rem",
        marginBottom: "1rem"
    },
    avatar: {
        position: "relative",
        marginTop: "2rem",
        marginBottom: "1rem",
        cursor: "pointer",
        width: "120px",
        height: "120px",
        border: "0.2rem solid #A0A0A0",
    },
    cameraAvatarinA: {
        cursor: "pointer",
        width: "30px",
        height: "30px",
        marginLeft: '-25px',
        marginBottom: '-65px',
        border: "0.1rem solid #67CEAC",
        backgroundColor: 'black'
    },
    avatarContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    }
  }));

function SignupPage(props) {
    const classes = useStyles();
    return (
        <div>
                <div  className={classes.avatarContainer}>
                    <Avatar className={classes.avatar}></Avatar>
                    <Avatar className={classes.cameraAvatarinA}>
                        <CameraAltIcon/>
                    </Avatar>
                    
                </div>
                
                <SignupForm />
                <div className={classes.fieldStyles}>
                    Already a user?
                </div>
                <div className={classes.fieldStyles}>
                <DividerWithText>OR</DividerWithText>
                </div>
                <div className={classes.fieldStyles}>
                    <Button type="signinWithGoogle" text="SIGNIN WITH GOOGLE"/>
                </div>
                <div className={classes.fieldStyles}>
                    <Button type="register" text="SIGNIN WITH FACEBOOK"/>   
                </div>
                <Divider variant="middle" style={{alignSelf: 'stretch',width:'100px'}} />
           
        </div>
    );
}

export default SignupPage;