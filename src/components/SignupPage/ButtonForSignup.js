const ButtonForSignup = (props) => {
    const { type, text } = props;
    const defaultButtonStyle = {
      display: 'flex',
      width: '200px',
      padding: '10px',
      textTransform: 'uppercase',
      borderRadius: '3px',
      marginButtom: '10px',
      justifyContent: 'center'
    }
    const colorOptions = {
        'register': { backgroundColor: '#7783FD', color:'white'},
        'signinWithGoogle': { backgroundColor: '#E15E7D', color:'white'},
        'signinWithFacebook': { backgroundColor: '#3B5899', color:'white'},
        // 'disabled': { backgroundColor: '#bbb', color:'#888484', pointerEvents: 'none', cursor: "not-allowed"},
    }
    const buttonStyle = Object.assign({}, defaultButtonStyle, colorOptions[type])
    return (
        <div style = {buttonStyle}>{text}</div>
    )
}
export default ButtonForSignup;

