import React from 'react';
import { Link } from 'react-router-dom'

function Header() {
    return (
        <div>
           <Link to="/" className="">
               Registration Page
           </Link>
           <Link to="/button" className="">
               Check Buttons
           </Link>
        </div>
    );
}

export default Header;